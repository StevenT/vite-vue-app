module.exports = {
  root: true,
  extends: [
    // add more generic rulesets here, such as:
    "plugin:@typescript-eslint/recommended", // Uses the recommended rules from the @typescript-eslint/eslint-plugin
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
    "plugin:prettier/recommended", // Enables eslint-plugin-prettier and displays prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
    // "prettier/@typescript-eslint", // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier
    "plugin:vue/vue3-recommended",
    // "prettier/vue",
    "prettier",
  ],
  parserOptions: {
    ecmaVersion: 2020, // Allows for the parsing of modern ECMAScript features
    sourceType: "module", // Allows for the use of imports,
    parser: "@typescript-eslint/parser", // Specifies the ESLint parser
    project: "./tsconfig.json",
    extraFileExtensions: [".vue"],
  },
  ignorePatterns: [".eslintrc.js"],
  rules: {
    "linebreak-style": ["error", "unix"],
    "@typescript-eslint/member-ordering": ["error"],
    "lines-between-class-members": ["error", "always", { exceptAfterSingleLine: true }],
    "@typescript-eslint/no-misused-promises": [
      "error",
      {
        checksVoidReturn: false,
      },
    ],
  },
};

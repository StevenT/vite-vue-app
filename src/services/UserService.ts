import { User } from "../models/User";

const baseUrl = "https://swapi.dev/api";
const id = 12;

export async function getUser(): Promise<User> {
  try {
    const res = await fetch(`${baseUrl}/people/${id}/`);
    return (await res.json()) as User;
  } catch (error) {
    const e = error as Error;
    e.message = `Error retrieving all users: ${e.message}`;
    throw e;
  }
}

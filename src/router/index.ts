import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "HomePage",
    component: () => import("../views/HomePage.vue"),
  },
  {
    path: "/about",
    name: "AboutPage",
    component: () => import("../views/AboutPage.vue"),
  },
  {
    path: "/:catchAll(.*)",
    component: () => import("../views/NotFoundPage.vue"),
  },
];

const index = createRouter({
  history: createWebHistory(),
  routes,
});

export default index;

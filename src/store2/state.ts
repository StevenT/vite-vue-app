import { User } from "../models/User";

export interface State {
  user: User | null;
}

export const state = {
  user: null,
};

import { GetterTree } from "vuex";
import { State } from "./state";
import { User } from "../models/User";

export type Getters = {
  user(state: State): User | null;
};

export const getters: GetterTree<State, State> & Getters = {
  user: (state) => state.user,
};

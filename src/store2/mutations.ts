import { MutationTree } from "vuex";
import { MutationTypes } from "./mutation-types";
import { State } from "./state";
import { User } from "../models/User";

export type Mutations<S = State> = {
  [MutationTypes.SET_USER](state: S, payload: User | null): void;
};

export const mutations: MutationTree<State> & Mutations = {
  [MutationTypes.SET_USER](state, payload) {
    state.user = payload;
  },
};

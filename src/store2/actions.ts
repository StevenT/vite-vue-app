import { ActionTree, Commit } from "vuex";
import { getUser } from "../services/UserService";
import { ActionTypes } from "./action-types";
import { MutationTypes } from "./mutation-types";
import { State } from "./state";
import { User } from "../models/User";

export interface Actions {
  [ActionTypes.LOAD_USER]({ commit }: { commit: Commit }): Promise<User>;
}

export const actions: ActionTree<State, State> = {
  async [ActionTypes.LOAD_USER]({ commit }) {
    let user: User;
    try {
      user = await getUser();
      commit(MutationTypes.SET_USER, user);
      return user;
    } catch (error) {
      console.error(error);
    }
  },
};
